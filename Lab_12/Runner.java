public class Runner
{
	public static void main(String[] args)
	{
		Point2D point1 = new Point2D(4, 3);
		Point2D point2 = new Point2D(4, 0);
		Point2D point3 = new Point2D(8, 0);
		Point2D origin = new Point2D(0, 0);

		Shape[] shapes = new Shape[2];
		shapes[0] = new Triangle(point1, point2, point3);

		shapes[1] = new Circle(origin, 1);

		shapes[1].moveVertical(100);
		shapes[1].moveHorizontal(90);
		shapes[0].moveVertical(98);
		shapes[0].moveHorizontal(89);

		System.out.printf("%20s\n\n", "--Shapes--");
		for(Shape shape: shapes)
		{	
			System.out.println(shape.toString());
		}
		
	}
}