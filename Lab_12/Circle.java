/**
 * Models a circle as a Point2D and a radius. 
 *
 * Author: Franklin D. Worrell
 * Revised: 18 July 2016 at 9:38
 * Assignment: Lab 9 - Polymorphism
 * Class: Circle
 */ 

public class Circle extends Shape {
	/* Stubbed */ 
	
	// Declare any additional instance variables a Circle will need. 
	private double area;
	private double radius;
	
	// Write an appropriate constructor for a circle. 
	public Circle() {
		super();
		this.radius = -1;
	}

	public Circle(Point2D center, double radius) {
		super(center);
		this.radius = radius;
	}
	
	// Write an appropriate area() method for a circle. 
	public double area() {

		this.area = Math.PI * Math.pow(radius, 2);

		return this.area;

	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	// Think about whether Shape.toString() will be adequate for Circle.
	// If not, write a toString() method for Circle. 
	public String toString() {
		String string = "\nCircle\n" + super.toString() + " r = " + this.radius+ "\narea  = " + this.area();
		return string;
	}

} // end class Triangle