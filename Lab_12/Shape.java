/**
 * Defines a collection of points and gives basic functionality to the 
 * collection including adding and deleting points, etc. 
 *
 * Author: Franklin D. Worrell
 * Revised: 18 July 2016 at 9:35
 * Assignment: Lab 9 - Polymorphism
 * Class: Shape
 */ 
 
import java.util.LinkedList; 

public abstract class Shape implements Moveable{

	private LinkedList<Point2D> points;	// LinkedList to hold points.
	
	
	/**
	 * Default constructor initializes an empty LinkedList of Point2D.
	 */ 
	public Shape() {
		this.points = new LinkedList<Point2D>(); 
	} // end default constructor
	
	
	/**
	 * Variable argument constructor will take a number of Point2Ds and 
	 * add each of them to the LinkedList of Point2D. 
	 *
	 * @param	initialPoints	array of Point2Ds to add to this
	 */ 
	public Shape(Point2D... initialPoints) {
		// Create the LinkedList with call to default constructor. 
		this(); 
		
		// Iterate through the Point2Ds and add them to the LinkedList. 
		for (int i = 0; i < initialPoints.length; i++) {
			this.points.add(initialPoints[i]); 
		} 
	} // end variable argument constructor 
	
	
	/**
	 * Returns the Point2D contained at the specified index in the 
	 * LinkedList
	 *
	 * @param	index	the index of the Point2D to return a reference to
	 * @return	a reference to the Point2D at the specified index
	 */ 
	public Point2D get(int index) {
		return this.points.get(index); 
	} // end method get
	
	public abstract double area();

	/**
	 * Returns the number of Point2Ds in this.
	 * 
	 * @return	the number of Point2Ds this contains
	 */ 
	public int size() {
		return this.points.size(); 
	} // end method size
	
	
	/**
	 * Returns a String representation of this. 
	 *
	 * @return	String representing this
	 */ 
	@Override 
	public String toString() {
		return "Set of points "+ this.points.toString(); 
	} // end method toString

	@Override
	public void moveVertical(int distance)
	{
		for(Point2D yPoints: points)
		{
		  yPoints.moveVertical(distance);
		}
	}
	
	@Override
	public void moveHorizontal(int distance)
	{
		for(Point2D xPoints: points)
		{
		  xPoints.moveHorizontal(distance);
		}
	}
} // end class SetOfPoints