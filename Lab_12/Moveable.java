public interface Moveable{

	public abstract void moveVertical(int arg);

	public abstract void moveHorizontal(int arg);

}