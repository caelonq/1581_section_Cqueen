public class DynamicArray
{
  // Private Variable(s)
  private String[] array;
  private int numberOfElements;
  private int currentIndex;

  // No-Args Constructor.
  public DynamicArray()
  {
  	this.array = new String[10];
  	this.numberOfElements = 0;
  	this.currentIndex = 0;
  }

  // 1-Arg (integer) Constructor.
  public DynamicArray(int size)
  {
  	this.array = new String[size];
  	this.numberOfElements = 0;
  	this.currentIndex = 0;
  }

  // 1-Arg (String Array) Constructor.
  public DynamicArray(String[] strings)
  {
  	this.array = new String[strings.length];
    for(int i = 0; i < strings.length; i++)
    {
      this.array[i] = strings[i]; 
    }
    this.numberOfElements = strings.length;
    this.currentIndex = strings.length;
  }

  // Getter Method -- Returns A String Using The Method's Arg` "index".
  public String getString(int index){ return this.array[index]; }

  // Boolean Method -- if The Array Is Empty Return True, else . . .False.
  public boolean isEmpty()
  {
    if(this.numberOfElements == 0)
      return true;
    else
      return false;
  }

  // Returns The Number Of Elements In The Array.
  public int sizeOf(){ return this.numberOfElements;}

  // Add String Method.
  public void add(String newString)
  {
    System.out.println("size of arrray: " + this.array.length);
    if(this.array.length == this.numberOfElements)
    { 
      this.expandArray();
    }
    this.array[currentIndex] = newString;
    this.numberOfElements++;
    this.currentIndex++;
  }

 /*****************************************************
 * Method To Complete Add Method . . . If Array Is Full-- 
 * Recursive Technique; AkA Helper Method-- expandArray().
 * Construct a newArray with a length == array.length * 2/3.
 * Add each of array's values to the newArray.
 * Set array to equal the expanded newArray containing 
 * the original values and additional space.
 ***************************************************/
  private void expandArray()
  { 
    int newLength = this.array.length * 3/2;
    String[] newArray = new String[newLength];
    for(int i = 0; i < this.array.length; i++)
    {
       newArray[i] = this.array[i];
    }
      this.array = newArray;
  }

  /*****************************************************
  * Remove String Method-- AkA Versatile Push/Pop Method.
  * Shift Remaining Elements To the Left One Index,
  * Overwriting The Removed Index Element To The Removed Index + 1 Element.
  * Returns The removedString.
  ******************************************************/
  public String remove(int index)
  { 
    String removedString = array[index];
    for(int k = index; k < this.array.length; k++)
      {
          if(k+1 < array.length)
          {
            array[k] = array[k+1];
          }
      }
    this.numberOfElements--;
    this.currentIndex--;
    return removedString;
  }
}

