public class PointDriver{
	public static void main(String[] args)
	{
		Point2D point1 = new Point2D(4 ,3);
		Point2D point2 = new Point2D(5 ,3);
		Point2D point3 = new Point2D(4 ,0);
		Point2D point4 = new Point2D(8, 0);


		SetOfPoints points = new SetOfPoints(point1, point2, point3, point4);
		System.out.println(points.toString());

		System.out.printf("\n\n%20s\n","--Distances-- ");

		System.out.println("Point 3 to 1: "+point3.distance(point1));
		System.out.println("Point 3 to 4: "+point3.distance(point4));
		System.out.println("Points 4 to 1: "+point4.distance(point1));
		System.out.println();

		Triangle triangle1 = new Triangle(point3, point4, point1);

		System.out.println("Triangle "+triangle1.toString());
		System.out.println("Triangle area: "+ triangle1.area());

		LineSegment lineSegment = new LineSegment(point2, point4);

		System.out.println("Line segment point 2 to 4: "+lineSegment.length());


	}
}