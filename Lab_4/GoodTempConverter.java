import java.util.Scanner; 

public class GoodTempConverter{
		private static double tempCelsius=0; 
		private static double tempFahr=0;
		private static int userChoice = 0;
		private static Scanner input = new Scanner(System.in);   
	public static void main(String[] args) {
		good_temp_Converter();
	} 
	public static void good_temp_Converter(){
	while (userChoice != 3) {
			menu();
			userChoice = input.nextInt(); 
			if (userChoice == 1) {
				tempconvc();
			} 
			else if (userChoice == 2) {
				tempconvf();
			} 
			else if (userChoice == 3) {
				System.out.println("Thanks! Have a nice day!");  
			} 
			else {
				System.out.println("Invalid input. Please enter only 1, 2, or 3."); 
			} 
		}
	}
	private static void menu(){
			System.out.println("Please select one of the following options: "); 
			System.out.println("	1. Convert Fahrenheit to Celsius; "); 
			System.out.println("	2. Convert Celsius to Fahrenheit; or "); 
			System.out.println("	3. Quit "); 
	}
	public static void tempconvc(){
				System.out.printf("Please enter the temperature in Celsius: "); 
				tempCelsius = input.nextDouble(); 
				tempFahr = tempCelsius * 1.8 + 32; 
				System.out.printf("%.2f degrees Celsius is equal to %.2f degrees Fahrenheit.%n", 
				tempCelsius, tempFahr); }
	public static void tempconvf(){
				System.out.printf("Please enter the temperature in Fahrenheit: "); 
				tempFahr = input.nextDouble(); 
				tempCelsius = (tempFahr - 32) / 1.8; 
				System.out.printf("%.2f degrees Fahrenheit is equal to %.2f degrees Celsius.%n",
				tempFahr, tempCelsius); 
	}
} 