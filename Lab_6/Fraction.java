import java.lang.Math;

public class Fraction
{
// Private Instance Variable(s).
  private double numerator;
  private double denominator;
  private double numeratorProduct;
  private double denominatorProduct;
  private double numeratorQuotient;
  private double denominatorQuotient;
  private double numeratorSum;
  private int divisible;
  private int max;
  public static boolean [] divisors;//= new boolean[10000]; 
  private double denominatorSum;
  private double numeratorDifference;
  private double denominatorDifference;
  private Fraction product;
  private Fraction quotient;
  private Fraction sum;
  private Fraction difference;

// Fraction No-argument Constructor defaults to Zero.
  public Fraction(){ this(0,0);}

// Fraction Single Argument Constructor Defaults To Zero || Undefined.
  public Fraction(double single){ this(0,0);}

// Fraction Consructor That Sets The numerator and denominator.
  public Fraction(double numerator, double denominator)
  {
	  this.denominator = denominator;
	  this.numerator = numerator;
  }

  // SETTERS!
  public void setNumerator(double newNumerator){ this.numerator = newNumerator; }
  public void setDenominator(double newDenominator){ this.denominator = newDenominator; }
  public void setFraction(double newNumerator, double newDenominator)
  {
    this.numerator = newNumerator;
    this.denominator = newDenominator;
  }

  // GETTERS !
  public double getNumerator(){ return this.numerator; }
  public double getDenominator(){ return this.denominator; }
  public Fraction getSum(){ return (this.sum != null? this.sum : this); }
  public Fraction getDifference(){ return (this.difference != null? this.difference : this );}
  public Fraction getProduct(){ return (this.product != null? this.product : this ); }
  public Fraction getQuotient(){return (this.quotient != null? this.quotient : this ); }
  public void getDivisors()
  {
    System.out.println("--Divisor List--");
    for (int variable = 0; variable < divisors.length; ++variable)
    {
      if (divisors[variable] == true)
      {
        System.out.printf("%d equals %B%n ", variable, divisors[variable]);
      }
    }
  }

  // Returns A Lexical Representation Of The Fraction In The Fraction's Traditonal Format (part/whole).
  public String toString()
  { 
    return String.format("Numerator: %.4f%nDenominator: %.4f%nFraction: %.4f/%.4f%n ", this.numerator, 
      this.denominator, this.numerator, this.denominator);
  }

  // Method To Multiply Fractions. Returns A Fraction Object And Changes The State Of the Instance Variables.
  public Fraction multiply(Fraction factor)
  {
   	numeratorProduct = numerator * factor.numerator;
   	denominatorProduct = denominator * factor.denominator;
    numerator = numeratorProduct;
    denominator = denominatorProduct;
   	product = new Fraction(numeratorProduct, denominatorProduct);
    return product;
  }

  // Method To Divide Fractions. Returns A Fraction Object And Changes The State Of the Instance Variables.
  public Fraction divide(Fraction divisor)
  {
   	numeratorQuotient = numerator * divisor.denominator;
   	denominatorQuotient = denominator * divisor.numerator;
    numerator = numeratorQuotient;
    denominator = denominatorQuotient;
     quotient = new Fraction(numeratorQuotient, denominatorQuotient);
   	return quotient;
  }

  // Method To Add Fractions. Returns A Fraction Object And Changes The State Of the Instance Variables.
  public Fraction add(Fraction addend)
  {
    numeratorSum = (addend.denominator * numerator) + (denominator * addend.numerator);
    denominatorSum = (denominator * addend.denominator);
    numerator = numeratorSum;
    denominator = denominatorSum;
    sum = new Fraction(numeratorSum, denominatorSum);
    return sum;
  }

  // Method To Subtract Fractions. Returns A Fraction Object And Changes The State Of the Instance Variables.
   public Fraction subtract(Fraction subtrahend)
  {
    numeratorDifference = (subtrahend.denominator * numerator) - (denominator * subtrahend.numerator);
    denominatorDifference = (denominator * subtrahend.denominator);
    numerator = numeratorDifference;
    denominator = denominatorDifference;
    difference = new Fraction(numeratorDifference, denominatorDifference);
    return difference;
  }

  // Method To Simplify Fractions.
  public void simplify()
  {
    if( numerator != denominator)
    { 
      max = (int) Math.max(this.denominator, this.numerator); 
    }else
    { 
      max = (int) numerator + 1;
    }
    divisors = new boolean[max];
  	for(int i = 0; i <= max ; ++i)
  	{
  		if(this.numerator%i == 0 && this.denominator%i == 0)
      {
        this.divisible = i;
        divisors[i]= true;
      }
  	}
    numerator = numerator / (double) divisible;
    denominator = denominator / (double) divisible;
  }
}/// Ends Fraction Class

class TestFractionClass
{
  public static void main(String[] args)
  {
    Fraction firstFraction = new Fraction(51.0, 34.0);
    firstFraction.simplify();
    System.out.println(firstFraction);
    firstFraction.getDivisors();



  }
}